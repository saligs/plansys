-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: plansis
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `p_audit_trail`
--

DROP TABLE IF EXISTS `p_audit_trail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `url` text,
  `description` text,
  `pathinfo` text,
  `module` text,
  `ctrl` text,
  `action` text,
  `params` text,
  `data` text,
  `stamp` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `form_class` varchar(255) DEFAULT NULL,
  `model_class` varchar(255) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `p_audit_trail_has_p_user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_audit_trail`
--

LOCK TABLES `p_audit_trail` WRITE;
/*!40000 ALTER TABLE `p_audit_trail` DISABLE KEYS */;
INSERT INTO `p_audit_trail` VALUES (1,'login','/plan/index.php?r=site%2Flogin','Logged in from 101.50.2.210','/site/login','','site','login','',NULL,'2017-05-23 12:09:12',1,'920a0c0a3dfa5ef28409d7dc42bb906f',NULL,NULL,NULL);
/*!40000 ALTER TABLE `p_audit_trail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_migration`
--

DROP TABLE IF EXISTS `p_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_migration`
--

LOCK TABLES `p_migration` WRITE;
/*!40000 ALTER TABLE `p_migration` DISABLE KEYS */;
INSERT INTO `p_migration` VALUES ('m000000_000000_base',1495516108),('m160112_125146_p_user',1495516108),('m160112_130826_p_role',1495516108),('m160112_132000_p_user_role',1495516108),('m160112_134617_p_audit_trail',1495516108);
/*!40000 ALTER TABLE `p_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_role`
--

DROP TABLE IF EXISTS `p_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `role_description` varchar(255) NOT NULL,
  `menu_path` varchar(255) DEFAULT NULL,
  `home_url` varchar(255) DEFAULT NULL,
  `repo_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_role`
--

LOCK TABLES `p_role` WRITE;
/*!40000 ALTER TABLE `p_role` DISABLE KEYS */;
INSERT INTO `p_role` VALUES (1,'dev','IT - Developer',NULL,'/docs/welcome',NULL);
/*!40000 ALTER TABLE `p_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_user`
--

DROP TABLE IF EXISTS `p_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_user`
--

LOCK TABLES `p_user` WRITE;
/*!40000 ALTER TABLE `p_user` DISABLE KEYS */;
INSERT INTO `p_user` VALUES (1,'dev@company.com','dev-admin','$2y$10$HBnmmDJIstzTXpHV6JNMFe.bBMw3ZxVnQEYmFOhUspIERE1S0pGv6',NULL,0);
/*!40000 ALTER TABLE `p_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_user_role`
--

DROP TABLE IF EXISTS `p_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_default_role` varchar(255) DEFAULT 'No',
  PRIMARY KEY (`id`),
  KEY `p_user_has_p_role` (`user_id`),
  KEY `p_role_has_p_user` (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_user_role`
--

LOCK TABLES `p_user_role` WRITE;
/*!40000 ALTER TABLE `p_user_role` DISABLE KEYS */;
INSERT INTO `p_user_role` VALUES (1,1,1,'Yes');
/*!40000 ALTER TABLE `p_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-23 12:11:40
